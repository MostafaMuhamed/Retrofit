package com.example.retrofit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.retrofit.Model.UserModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
{
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    DividerItemDecoration dividerItemDecoration;
    userAdapter adapter;
    Retrofit retrofit;
    Helper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false);
        dividerItemDecoration = new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(dividerItemDecoration);

        retrofit = new Retrofit
                .Builder()
                .baseUrl("https://jsonplaceholder.typicode.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        helper = retrofit.create(Helper.class);

        getPost();
    }

    private void getPost()
    {
      Call<ArrayList<UserModel>> call = helper.getHelper();
      call.enqueue(new Callback<ArrayList<UserModel>>()
      {
          @Override
          public void onResponse(Call<ArrayList<UserModel>> call, Response<ArrayList<UserModel>> response)
          {
              if (response.isSuccessful())
              {
               ArrayList<UserModel> userModels = response.body();
               adapter = new userAdapter(userModels);
               recyclerView.setAdapter(adapter);
              }
          }

          @Override
          public void onFailure(Call<ArrayList<UserModel>> call, Throwable t)
          {
              Toast.makeText(MainActivity.this, "No Network & Empty List", Toast.LENGTH_SHORT).show();
          }
      });
    }

    public class userAdapter extends RecyclerView.Adapter<userAdapter.userVH>
    {
        ArrayList<UserModel> userModels;

        public userAdapter(ArrayList<UserModel> userModels)
        {
            this.userModels = userModels;
        }

        @NonNull
        @Override
        public userVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.item,null);
            return new userVH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull userVH holder, int position)
        {
          UserModel model = userModels.get(position);
          holder.postID.setText(model.getPostId()+"");
          holder.ID.setText(model.getId()+"");
          holder.Name.setText(model.getName());
          holder.Email.setText(model.getEmail());
          holder.Body.setText(model.getBody());
        }

        @Override
        public int getItemCount()
        {
            return userModels.size();
        }

        public class userVH extends RecyclerView.ViewHolder
        {
            TextView postID,ID,Name,Email,Body;
            public userVH(@NonNull View itemView)
            {
                super(itemView);

                postID = itemView.findViewById(R.id.txt_postid);
                ID = itemView.findViewById(R.id.txt_id);
                Name = itemView.findViewById(R.id.txt_name);
                Email = itemView.findViewById(R.id.txt_email);
                Body = itemView.findViewById(R.id.txt_body);
            }
        }
    }
}
